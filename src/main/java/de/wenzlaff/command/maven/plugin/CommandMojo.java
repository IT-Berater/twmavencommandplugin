package de.wenzlaff.command.maven.plugin;

import javax.inject.Inject;

import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.project.MavenProject;

/**
 * Ein Kommando-Mojo zum ausführen von Kommandozeilen-Befehlen.
 * 
 * Das Ergebnis wird ins Log geschrieben.
 * 
 * @author Thomas Wenzlaff
 */
@Mojo(name = "info", defaultPhase = LifecyclePhase.INITIALIZE)
public class CommandMojo extends AbstractMojo {

	@Inject
	private CommandProvider commandProvider;

	/**
	 * Der command Parameter der ausgeführt wird.
	 * 
	 * Wenn er nicht angegeben wird, wird default: ls -la verwendet.
	 * 
	 * Es kann aber auch noch ein Verzeichnis übergeben werden, das überwacht werden
	 * soll. Z.B.
	 * 
	 * mvn de.wenzlaff.command.maven.plugin:de.wenzlaff.command.maven.plugin:info
	 * -Dcommand="ls -la /Users/thomaswenzlaff"
	 * 
	 * mvn
	 * de.wenzlaff.command.maven.plugin:de.wenzlaff.command.maven.plugin:0.0.2-SNAPSHOT:info
	 * -Dloglevel="error
	 */
	@Parameter(property = "command", defaultValue = "ls -la")
	private String command;

	@Parameter(property = "project", readonly = true)
	private MavenProject project;

	/**
	 * Gibt den Loglevel an. Default loglevel = info
	 * 
	 * Möglich Werte sind: debug, warn, info, error
	 */
	@Parameter(property = "loglevel", defaultValue = "info")
	private String loglevel;

	@Override
	public void execute() throws MojoExecutionException, MojoFailureException {

		// mvn ${groupId}:${artifactId}:${goal}
		// mvn de.wenzlaff.command.maven.plugin:de.wenzlaff.command.maven.plugin:info

		getLog().info("Führe das Kommando aus: " + command + ", mit Loglevel: " + loglevel);

		String commandErgebnis = commandProvider.getCommand(command);

		log(commandErgebnis);
	}

	private void log(String ausgabe) {

		if (loglevel.equalsIgnoreCase("debug")) {
			getLog().debug(ausgabe);
		} else if (loglevel.equalsIgnoreCase("warn")) {
			getLog().warn(ausgabe);
		} else if (loglevel.equalsIgnoreCase("error")) {
			getLog().error(ausgabe);
		} else {
			getLog().info(ausgabe);
		}
	}
}