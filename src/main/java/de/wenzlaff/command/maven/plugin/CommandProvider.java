package de.wenzlaff.command.maven.plugin;

import org.apache.maven.plugin.MojoExecutionException;

/**
 * Command Provider Interface.
 * 
 * @author Thomas Wenzlaff
 */
interface CommandProvider {
	/**
	 * Führt das Kommando aus und liefert das Ergebnis zurück.
	 * 
	 * @param command Kommandozeilen Befehl
	 * @return Ergebnis
	 * @throws MojoExecutionException bei Fehlern
	 */
	String getCommand(String command) throws MojoExecutionException;
}