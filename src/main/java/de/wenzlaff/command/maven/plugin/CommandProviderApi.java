package de.wenzlaff.command.maven.plugin;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import javax.inject.Named;
import javax.inject.Singleton;

import org.apache.maven.plugin.MojoExecutionException;

/**
 * Command Provider.
 * 
 * @author Thomas Wenzlaff
 */
@Named
@Singleton
public class CommandProviderApi implements CommandProvider {

	@Override
	public String getCommand(String command) throws MojoExecutionException {

		String input = null;
		StringBuilder buffer = new StringBuilder("\n");

		try {
			Process p = Runtime.getRuntime().exec(command);

			try (BufferedReader stdInput = new BufferedReader(new InputStreamReader(p.getInputStream()))) {

				while ((input = stdInput.readLine()) != null) {
					buffer.append(input);
					buffer.append("\n");
				}
			}
			try (BufferedReader stdError = new BufferedReader(new InputStreamReader(p.getErrorStream()))) {

				while ((input = stdError.readLine()) != null) {
					buffer.append("Error: " + input);
				}
			}
		} catch (IOException e) {
			throw new MojoExecutionException("Fehler beim ausführen des Kommandos: '" + command, e);
		}
		return buffer.toString();
	}
}